#pragma once
#include<string>

using FString = std::string;
using int32 = int;

struct FBullCowCount {
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum EWordStatus {
	Invalid_Status,
	OK,
	Not_Isogram,
	Wrong_Length,
	Not_Lowercase
};

class FBullCowGame {

public:
	FBullCowGame(); //konstruktor

	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetHiddenWordLength() const;
	bool IsGameWon() const;

	void Reset();
	EWordStatus CheckGuessValidity(FString word) const;
	FBullCowCount SubmitValidGuess(FString);

private:
	bool bGameIsWon;
	int32 MyCurretntTry;
	int32 MyMaxTries;
	FString MyHiddenWord;


	bool IsIsogram(FString) const;
	bool IsLowercase(FString) const;
};