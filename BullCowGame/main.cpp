#include <iostream>
#include <string>
#include "FBullCowGame.h"

using FText = std::string;
using int32 = int;

void PrintIntro();
void PlayGame();
FText GetValidGuess();
bool AskToPlayAgain();
void PrintGameSummary();





FBullCowGame BCGame;

int main() {
	bool bPlayAgain = false;

	do {
		PrintIntro();
		PlayGame();
		bPlayAgain = AskToPlayAgain();
	} while (bPlayAgain);

	return 0;
}

void PlayGame(){

	int32 MaxTries = BCGame.GetMaxTries();
	BCGame.Reset();

	//TODO change from FOR to WHILE loop once we are validating tries
	while(!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= MaxTries) {
		FText guess = GetValidGuess(); //TODO make loop checking valid
		

		//submit valid guess to the game
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(guess);


		std::cout << "Bulls = " << BullCowCount.Bulls;
		std::cout << "Cows = " << BullCowCount.Cows << "\n\n";

	}

	PrintGameSummary();

	return;
}

void PrintIntro() {

	std::cout << "\n\nWelcome tto Bulls and Cows, a fun word game!" << std::endl;
	std::cout << "Can you guess the " << BCGame.GetHiddenWordLength() << " letter isogram I'm thinking of?\n\n";

	return;
}

FText GetValidGuess() {

	FText Guess = "";
	EWordStatus Status = EWordStatus::Invalid_Status;

	do {
		int32 CurrentTry = BCGame.GetCurrentTry();

		std::cout << "Try: " << CurrentTry << ". Enter your guess: ";
		getline(std::cin, Guess);

		Status = BCGame.CheckGuessValidity(Guess);

		switch (Status) {
		case EWordStatus::Wrong_Length:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n";
			break;
		case EWordStatus::Not_Isogram:
			std::cout << "Please enter a isogram!.\n";
			break;
		case EWordStatus::Not_Lowercase:
			std::cout << "Please enter all lower letters!.\n";
			break;

		default:
			//assume the guess is valid
			break;
		}
		std::cout << "\n";
	} while (Status != EWordStatus::OK);
	return Guess;
}

bool AskToPlayAgain(){
	std::cout << "Do you want to play again?";
	FText Response = "";
	getline(std::cin, Response);

	return (Response[0] == 'y') || (Response[0] == 'Y');
}

void PrintGameSummary() {
	if (BCGame.IsGameWon()) {
		std::cout << "YOY WIN!\n\n";
	}
	else {
		std::cout << "YOY LOSE!\n\n";
	}
}
